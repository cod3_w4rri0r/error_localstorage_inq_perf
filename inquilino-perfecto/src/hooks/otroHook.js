import { useEffect, useState } from 'react';

export const otroHook = (key) => {
  const [valor, setValor] = useState(localStorage.getItem(key) ?? '');
  useEffect((key) => {
    setValor(key);
    console.log(valor);
  }, []);
};
