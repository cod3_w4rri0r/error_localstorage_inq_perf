import './App.css';
import React, { useEffect, useState } from 'react';
import Header from './components/Header';
import Home from './Home';
import Login from './components/Login';
import About from './components/About';
import Terms from './components/Terms';
import PrivacyPolicy from './components/PrivacyPolicy';
import Contact from './pages/Contact';
import { Route, Routes } from 'react-router-dom';
import Register from './components/Register';
import PassRecovery from './components/PassRecovery';
//import useHook from './hooks/myHook';

function App() {
  console.log('Se ejecutó el return de App');
  // const [cities, setCities] = useState([]);
  // const [search, setSearch] = useState('');

  // const { value, updateValue, PI } = useHook();

  // useEffect(() => {
  //   //imaginemos que esto es una petición asíncrona al backend
  //   setCities([
  //     'A Coruña',
  //     'Lugo',
  //     'Ourense',
  //     'Pontevedra',
  //     'Barcelona',
  //     'Girona',
  //     'Donostia',
  //     'Pamplona',
  //     'Lleida',
  //     'Teruel',
  //     'Zaragoza',
  //     'Oviedo',
  //     'Huesca',
  //     'Gipuzkoa',
  //   ]);
  // }, []);

  // const searchResults =
  //   search !== ''
  //     ? cities.filter((city) =>
  //         city.toLowerCase().includes(search.toLocaleLowerCase())
  //       )
  //     : [];

  // console.log(searchResults);
  return (
    <div>
      <Header />
      {/* <h1>{value}</h1>
      <h2>{PI}</h2>
      <button onClick={() => updateValue('Berto')}>
        Cambiar nombre por Berto
      </button>
      <section className='autocomplete'>
        <input
          type='search'
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        {searchResults.length ? (
          <ul>
            {searchResults.map((result) => (
              <li key={result}>{result}</li>
            ))}
          </ul>
        ) : null}
      </section> */}

      <Routes>
        <Route path='/' element={<Home />} />
        <Route
          path='login'
          element={<Login url='http://localhost:4000/users/login' />}
        />
        <Route path='/register' element={<Register />} />
        <Route path='/about' element={<About />} />
        <Route path='/terms' element={<Terms />} />
        <Route path='/privacy' element={<PrivacyPolicy />} />
        <Route path='/contact' element={<Contact />} />
        <Route path='/passwordRecovery' element={<PassRecovery />} />
      </Routes>
    </div>
  );
}

export default App;
