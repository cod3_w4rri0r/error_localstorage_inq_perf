import React from 'react';
import '../css/Card.css';

const Card = ({ colour, radius, shadow, children }) => {
  return (
    <div
      className='card'
      style={{
        backgroundColor: colour,
        borderRadius: radius,
        boxShadow: shadow,
      }}
    >
      {children}
    </div>
  );
};

export default Card;
