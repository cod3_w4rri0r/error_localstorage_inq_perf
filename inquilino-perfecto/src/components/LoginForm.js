import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { TokenContext } from '..';
import '../css/LoginForm.css';

const LoginForm = ({ url }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [token, setToken] = useContext(TokenContext);

  const handleEmail = (e) => {
    console.log('Email:', e.target.value);
    e.preventDefault();
    setEmail(e.target.value);
  };

  const handlePassword = (e) => {
    e.preventDefault();
    console.log('Password: ', e.target.value);
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(url);
    console.log(e.target.value);
    console.log('Email -> ', email, 'Password -> ', password);
    try {
      const reqBody = {
        email: email,
        password: password,
      };
      console.log(reqBody);
      const res = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(reqBody),
      });
      const { data } = await res.json();
      console.log('res:', res);
      if (res.ok) console.log('token', data.token); // If there's an error, it gets caught autom. in catch clause
    } catch (error) {
      console.error(error);
    }
  };

  console.log('url', url);

  return (
    <form className='login-form' onSubmit={handleSubmit}>
      <label htmlFor='email'>
        Email: <input id='email' value={email} onChange={handleEmail}></input>
      </label>
      <label>
        Contraseña:{' '}
        <input
          id='password'
          type='password'
          value={password}
          onChange={handlePassword}
        />
      </label>
      <Link to='/passwordRecovery'>Olvidé mi contraseña</Link>
      <button>Iniciar sesión</button>
    </form>
  );
};

export default LoginForm;
