import React from 'react';
import Card from './Card';
import LoginForm from './LoginForm';

function Login({ url }) {
  return (
    <div>
      <h1>Login page</h1>
      <Card colour='white' radius={10} shadow={'2px 2px 5px grey'}>
        <LoginForm url={url} />
      </Card>
    </div>
  );
}

export default Login;
