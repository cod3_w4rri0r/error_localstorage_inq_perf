import '../css/Header.css';
import favicon from '../logo.svg';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

function Header() {
  const navigate = useNavigate();
  return (
    <header>
      <img
        className='logo'
        src={favicon}
        alt='logo'
        onClick={() => navigate('/')}
      />
      <h1>Inquilino Perfecto</h1>
      <Link to='/login'>Inicia Sesión</Link>
      <Link to='/register'>Regístrate</Link>
    </header>
  );
}

export default Header;
