import './App.css';
import Main from './components/Main';
import Footer from './components/Footer';
import React from 'react';

function Home() {
  console.log('Se ejecutó el return de Home');
  return (
    <div>
      <Main />
      <Footer />
    </div>
  );
}

export default Home;
